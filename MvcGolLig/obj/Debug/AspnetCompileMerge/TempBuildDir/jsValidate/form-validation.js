﻿// Wait for the DOM to be ready
$(function () {
    // Initialize form validation on the registration form.
    // It has the name attribute "registration"
    $("form[name='registration']").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
            adSoyad: "required", 
            telefonNo:"required",
            email: {
                required: true, 
                email: true
            },
            Sifre: {
                required: true,
                minlength: 5,
                equalTo: "#Sifre2"
            },
            Sifre2: {
                required: true,
                minlength: 5
            }
        },
        // Specify validation error messages
        messages: {
            adSoyad: "Lütfen adınızı giriniz.",
            telefonNo: "Lütfen telefon numaranızı giriniz.",
            Sifre: {
                required: "Lütfen şifrenizi giriniz.",
                minlength: "Şifre 5 karakterden uzun olmak zorundadır.",
                equalTo: "Şifreler uyuşmuyor."
            },
            Sifre2: {
                required: "Lütfen şifrenizi giriniz.",
                minlength: "Şifre 5 karakterden uzun olmak zorundadır.",
                equalTo: "Şifreler uyuşmuyor."
            },
            email: "Please enter a valid email address"
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });
});