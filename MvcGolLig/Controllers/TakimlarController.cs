﻿using MvcGolLig.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcGolLig.Controllers
{
    public class TakimlarController : Controller
    {
        vtGolLigEntities vt = new vtGolLigEntities();
        // GET: Takimlar
        public ActionResult Index(string Sehirler, string Ilceler, string TakimDurumlari)
        {
            sehirleriYukle();
            IlcerleriGetir(0);
            takimDurumlariYukle();

            try
            {
                int pSehir = -1, pIlce = -1, pDurum = -1;

                if (Sehirler != null)
                {
                    pSehir = int.Parse(Sehirler);
                    IlcerleriGetir(pSehir);
                }
                if (Ilceler != null)
                {
                    pIlce = int.Parse(Ilceler);
                }
                if (TakimDurumlari != null)
                {
                    pDurum = int.Parse(TakimDurumlari);
                }

                if (pSehir != -1 && pIlce == -1 && pDurum == -1)
                {
                    return View(vt.tblKullanici.Where(x => x.gorunsunMu == 1 && x.il == pSehir).ToList());
                }
                else if (pSehir != -1 && pIlce != -1 && pDurum == -1)
                {
                    return View(vt.tblKullanici.Where(x => x.gorunsunMu == 1 && x.il == pSehir && x.ilce == pIlce).ToList());
                }
                else if (pSehir != -1 && pIlce == -1 && pDurum != -1)
                {
                    return View(vt.tblKullanici.Where(x => x.gorunsunMu == 1 && x.il == pSehir && x.takimDurumu == pDurum).ToList());
                }
                else if (pSehir != -1 && pIlce != -1 && pDurum != -1)
                {
                    return View(vt.tblKullanici.Where(x => x.gorunsunMu == 1 && x.il == pSehir && x.ilce == pIlce && x.takimDurumu == pDurum).ToList());
                }
            }
            catch (Exception ex)
            {
                return View(vt.tblKullanici.Where(x => x.gorunsunMu == 1).ToList());
            }

            return View(vt.tblKullanici.Where(x=>x.gorunsunMu==1).ToList());
        }

        private void takimDurumlariYukle()
        {
            List<SelectListItem> takimDurumList = new List<SelectListItem>();
            takimDurumList.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });
            foreach (var item in vt.tblTakimDurumu.ToList())
            {
                takimDurumList.Add(new SelectListItem { Text = item.aciklama, Value = item.takimDurumuId.ToString() });
            }
            ViewBag.TakimDurumlari = takimDurumList;
        }

        void sehirleriYukle()
        {
            List<SelectListItem> sehirList = new List<SelectListItem>();
            //sehirList.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });
            foreach (var item in vt.iller.ToList().OrderBy(x => x.sehir))
            {
                sehirList.Add(new SelectListItem { Text = item.sehir, Value = item.id.ToString() });
            }
            ViewBag.Sehirler = sehirList;
        }

        public ActionResult IlcerleriGetir(int id = 0)
        {
            List<SelectListItem> l = new List<SelectListItem>();
            l.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });

            foreach (var item in vt.ilceler.Where(x => x.sehir == id).ToList().OrderBy(x => x.ilce))
            {
                l.Add(new SelectListItem { Text = item.ilce, Value = item.id.ToString() });
            }

            ViewBag.Ilceler = l;
            return View();
        }


    }
}