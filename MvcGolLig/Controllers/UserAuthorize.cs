﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcGolLig.Controllers
{
    public class UserAuthorize : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Request.Cookies["golliglogin"] != null)
            {
                return true;
            }
            else
            {
                httpContext.Response.Redirect("/Sahalar"); 
                return false;
            }

        }
    }
}