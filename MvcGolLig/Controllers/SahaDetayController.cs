﻿using MvcGolLig.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace MvcGolLig.Controllers
{
    public class SahaDetayController : Controller
    {
        vtGolLigEntities vt = new vtGolLigEntities();
        // GET: SahaDetay
        /*
        [Route("SahaDetay")]
        public ActionResult Index(int sahaId = -1, string Tarih="", string kod="")
        {
            try
            {
                if (Session["golliglogin"] == null)
                {
                    Session.Add("back", sahaId);
                    Session.Add("backtarih", Tarih);

                    return Redirect("/giris");
                }
            }
            catch (Exception)
            { return Redirect("/giris"); }


            ViewBag.TarihYaz = DateTime.Now.ToShortDateString();
            if (kod!="")
            {
                ViewBag.Scroll = "row_" + kod;
            }

            try
            {

                if (sahaId == -1)
                {
                    return Redirect("/");                      
                }
                else
                {
                    if (Tarih != "")
                    {
                        DateTime pTarih = DateTime.Parse(Tarih);
                        ViewBag.TarihYaz = pTarih.ToShortDateString();

                        TimeSpan s = pTarih - DateTime.Now;
                        if ( (pTarih.ToShortDateString()==DateTime.Now.ToShortDateString()) || (s.TotalDays<=10 && s.TotalDays>0) )
                        {
                            //var rezler = vt.tblRezervasyon.Where(r => SqlFunctions.DateDiff("Day", r.tarih, pTarih) == 0 && r.durum != -1 && r.saha == id).Select(e => e.fiyatId);
                            //List<vw_SahaDetay> list = vt.vw_SahaDetay.Where(x => !rezler.Contains(x.KOD) && x.sahaId == id).ToList();

                            var list = vt.fnc_sahaBazliRezFiyat(sahaId, pTarih.ToShortDateString()).ToList();
                            if (list.Count > 0)
                            {
                                return View(list);
                            }
                            else
                            {
                                return Redirect("/");
                            }
                        }
                        else
                        {
                            ViewBag.StartupScript = "alert('Geçerli bir tarih giriniz.En fazla 10 gün ilerisi için randevu yapabilirsiniz.');";
                            var list = vt.fnc_sahaBazliRezFiyat(sahaId, DateTime.Now.ToShortDateString()).ToList();
                            return View(list);
                        } 
                    }
                    else
                    {
                        var list = vt.fnc_sahaBazliRezFiyat(sahaId, DateTime.Now.ToShortDateString()).ToList();

                        if (list.Count > 0)
                        {
                            return View(list);
                        }
                        else
                        {
                            return Redirect("/");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Redirect("/");
            } 
        }
        */


        //[Route("SahaDetay/{sahaAdi}")] 
        public ActionResult Index(string sahaAdi = "", string Tarih = "", string kod = "")
        {
            sahaAdi = sahaAdi.Replace("-", " ");

            var saha = vt.tblSaha.Where(x => x.adi == sahaAdi).FirstOrDefault();
            int id = -1;
            if (saha != null)
            {
                id = saha.sahaId;

                ViewBag.title_il = saha.iller.sehir;
                ViewBag.title_ilce = saha.ilceler.ilce;
                ViewBag.TipAciklama = (saha.tblSahaTipi != null) ? saha.tblSahaTipi.aciklama : "";
            }
            /*
            try
            {
                if (Session["golliglogin"] == null)
                {
                    Session.Add("back", id);
                    Session.Add("backtarih", Tarih);

                    return Redirect("/giris");
                }
            }
            catch (Exception)
            { return Redirect("/giris"); }
            */

            ViewBag.TarihYaz = DateTime.Now.ToShortDateString();
            if (kod != "")
            {
                ViewBag.Scroll = "row_" + kod;
            }

            try
            { 
                if (id == -1)
                {
                    return Redirect("/");
                }
                else
                {
                    if (Tarih != "")
                    {
                        DateTime pTarih = DateTime.Parse(Tarih);
                        ViewBag.TarihYaz = pTarih.ToShortDateString();

                        TimeSpan s = pTarih - DateTime.Now;
                        if ((pTarih.ToShortDateString() == DateTime.Now.ToShortDateString()) || (s.TotalDays <= 10 && s.TotalDays > 0))
                        {
                            //var rezler = vt.tblRezervasyon.Where(r => SqlFunctions.DateDiff("Day", r.tarih, pTarih) == 0 && r.durum != -1 && r.saha == id).Select(e => e.fiyatId);
                            //List<vw_SahaDetay> list = vt.vw_SahaDetay.Where(x => !rezler.Contains(x.KOD) && x.sahaId == id).ToList();

                            var list = vt.fnc_sahaBazliRezFiyat(id, pTarih.ToShortDateString()).ToList();
                            if (list.Count > 0)
                            {
                                return View(list);
                            }
                            else
                            {
                                return Redirect("/");
                            }
                        }
                        else
                        {
                            ViewBag.StartupScript = "alert('Geçerli bir tarih giriniz.En fazla 10 gün ilerisi için randevu yapabilirsiniz.');";
                            var list = vt.fnc_sahaBazliRezFiyat(id, DateTime.Now.ToShortDateString()).ToList();
                            return View(list);
                        }
                    }
                    else
                    {
                        var list = vt.fnc_sahaBazliRezFiyat(id, DateTime.Now.ToShortDateString()).ToList();

                        if (list.Count > 0)
                        {
                            return View(list);
                        }
                        else
                        {
                            return Redirect("/");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Redirect("/");
            } 
        }













       // [UserAuthorize]
        public JsonResult Kirala(string fiyatId,string sahaId, string tarih="", string saat="", string fiyat="")
        {
            DateTime pTarih = DateTime.Now;
            
            if (tarih!="")
            {
                pTarih = DateTime.Parse(tarih);
            }
            try
            {
                
                int pSahaId = int.Parse(sahaId);
                int pFiyatId = int.Parse(fiyatId);
                int pFiyat = int.Parse(fiyat);

                var saha = vt.tblSaha.Where(x => x.sahaId == pSahaId).FirstOrDefault();

                if (Session["golliglogin"] != null && saha!=null)
                {
                    
                    if (true)
                    {

                        var kontrol=vt.tblRezervasyon.Where(x => x.saha == pSahaId && x.fiyatId == pFiyatId && x.tarih == pTarih && x.durum != -1).FirstOrDefault();
                        if (kontrol==null)
                        {
                            string saatBas = saat.Split('-')[0];

                            double saatFarki = ((DateTime.Parse(saatBas)) - DateTime.Now).TotalHours;
                            TimeSpan GunFarki = DateTime.Now.Subtract(pTarih);

                            if (pTarih.ToShortDateString() == DateTime.Now.ToShortDateString() )
                            {
                                if (saatFarki > 0)
                                {//bugüne kayıt yapılacak saati kontrol et
                                    tblRezervasyon r = new tblRezervasyon();
                                    r.takim = int.Parse(Session["golliglogin"].ToString());
                                    r.saha = pSahaId;
                                    r.tarih = pTarih;
                                    r.fiyatId = pFiyatId;
                                    r.durum = 0;
                                    r.fiyat = pFiyat;

                                    vt.tblRezervasyon.Add(r);
                                    vt.SaveChanges();

                                    sahaSahibineMailGonder(pSahaId, int.Parse(Session["golliglogin"].ToString()), pTarih);

                                    Redirect("/profil");
                                    return Json(true, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json("Geçmiş saate rezervasyon alamazsınız!", JsonRequestBehavior.AllowGet);
                                } 
                            }
                            else if (GunFarki.TotalDays >= 1)
                            { 
                                return Json("Geçmiş saate rezervasyon alamazsınız!", JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                tblRezervasyon r = new tblRezervasyon();
                                r.takim = int.Parse(Session["golliglogin"].ToString());
                                r.saha = pSahaId;
                                r.tarih = pTarih;
                                r.fiyatId = pFiyatId;
                                r.durum = 0;
                                r.fiyat = pFiyat;

                                vt.tblRezervasyon.Add(r);
                                vt.SaveChanges();

                                sahaSahibineMailGonder(pSahaId, int.Parse(Session["golliglogin"].ToString()), pTarih);

                                Redirect("/profil");
                                return Json(true, JsonRequestBehavior.AllowGet);
                            } 
                        }
                        else
                        {
                            return Json("Bu rezervasyon daha önce yapılmış!", JsonRequestBehavior.AllowGet);
                        }                        
                    }
                    else
                    { 
                        return Json("Güvenlik doğrulaması başarısız!", JsonRequestBehavior.AllowGet);
                    }
                    
                }
                else
                {
                    if (saha!=null)
                    {
                        Session.Add("back", saha.adi);
                        Session.Add("backtarih", tarih);
                        Session.Add("backkod", fiyatId);
                    }

                    Redirect("/giris"); 
                    return Json("login", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                Redirect("/");
                return Json(false, JsonRequestBehavior.AllowGet);
            } 
        }

        private void sahaSahibineMailGonder(int SahaId, int kulid, DateTime Tarih)
        {
            try
            {
                var sahaSahibi = vt.tblSahaKullanici.Where(d => d.saha.Value == SahaId).ToList().Select(x => x.panelKullanici.Value);

                var sahipler = vt.tblKullanici.Where(x => sahaSahibi.Contains(x.kullaniciId)).ToList();

                var body = "";
                var message = new MailMessage();

                foreach (var item in sahipler)
                {
                    message.To.Add(new MailAddress(item.yetkiliMail));
                }
                 
                message.From = new MailAddress("info@gollig.com");
                 body = "<p>GolLig.com üzerinden yeni bir rezervasyonun var!</p>" + "<p>" + Tarih.ToString("dd.MM.yyyy") +" tarihli rezervasyonun detaylarına GolLig.com üzerinden ulaşabilir, onaylayabilir veya iptal edebilirsin.</p>" +
                   "<p>www.gollig.com</p>";
                 
                message.Subject = "GolLig | Yeni Rezervasyon!";
                 
                message.Body = (body);
                message.IsBodyHtml = true;

                NetworkCredential netCred = new NetworkCredential("info@gollig.com", "GolLig2016");

                SmtpClient smtpobj = new SmtpClient("gollig.com");
                smtpobj.EnableSsl = false;
                smtpobj.Credentials = netCred;
                message.IsBodyHtml = true;


                smtpobj.Send(message); 

            }
            catch (Exception ex)
            {
                 
            }


        }
    }
}