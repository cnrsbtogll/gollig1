﻿using MvcGolLig.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MvcGolLig.Controllers
{
    public class GirisController : Controller
    {
        // GET: Giris
        vtGolLigEntities vt = new vtGolLigEntities();

        public ActionResult Index(string txtGirisTelefon, string txtGirisSifre)
        {
            sehirleriYukle();
            takimDurumlariYukle();
            IlcerleriGetir(0);
            if (txtGirisTelefon!=null)
            {
                txtGirisTelefon = txtGirisTelefon.Replace("(", "");
                txtGirisTelefon = txtGirisTelefon.Replace(")", "");
                txtGirisTelefon = txtGirisTelefon.Replace("-", "");
                txtGirisTelefon = txtGirisTelefon.Replace(" ", "");
            }

            try
            {
                if (Session["golliglogin"]!=null)
                {
                    return Redirect("/");
                }
            }
            catch (Exception)
            { 
            }

            if (txtGirisTelefon==null || txtGirisSifre==null)
            {
                return View();
            }
            
                var k = vt.tblKullanici.Where(x => x.telefonNo==txtGirisTelefon && x.sifre == txtGirisSifre && x.aktif == 1).FirstOrDefault();
                if (k != null)
                {
                    Session.Add("golliglogin", k.kullaniciId);
                    if (k.admin == 1)
                    {
                        Session.Add("golliglogintip", "0");
                    }
                    else
                    {
                        Session.Add("golliglogintip", "100");
                    }

                        try
                        {
                            return RedirectToAction("index", "sahadetay", new { sahaAdi = Session["back"],Tarih=Session["backtarih"],kod=Session["backkod"] });
                        }
                        catch (Exception)
                        { return Redirect("/"); }                     
                }                
            
            return View();           
            
        }

        private void takimDurumlariYukle()
        {
            List<SelectListItem> takimDurumList = new List<SelectListItem>();
            //takimDurumList.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });
            foreach (var item in vt.tblTakimDurumu.ToList())
            {
                takimDurumList.Add(new SelectListItem { Text = item.aciklama, Value = item.takimDurumuId.ToString() });
            }
            ViewBag.TakimDurumlari = takimDurumList;
        }
        void sehirleriYukle()
        {
            List<SelectListItem> sehirList = new List<SelectListItem>();
            //sehirList.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });
            foreach (var item in vt.iller.ToList())
            {
                sehirList.Add(new SelectListItem { Text = item.sehir, Value = item.id.ToString() });
            }
            ViewBag.Sehirler = sehirList;
        }

        public ActionResult IlcerleriGetir(int id = 0)
        {
            List<SelectListItem> l = new List<SelectListItem>();
            l.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });
            foreach (var item in vt.ilceler.Where(x => x.sehir == id).ToList())
            {
                l.Add(new SelectListItem { Text = item.ilce, Value = item.id.ToString() });
            } 
            ViewBag.Ilceler = l;
            return View();
        }






        public ActionResult Kayit(string adSoyad,string sifre,string sifre2,string telefonNo,string mailadresi,string takimAdi,
            string aciklama,string TakimDurumlari, string Sehirler,string Ilceler,string listedeGorunsun,
            HttpPostedFileBase resim)
        {
            sehirleriYukle();
            takimDurumlariYukle();
            IlcerleriGetir(0);
            telefonNo = telefonNo.Replace("(", "");
            telefonNo = telefonNo.Replace(")", "");
            telefonNo = telefonNo.Replace("-", "");
            telefonNo = telefonNo.Replace(" ", "");

            Helpers.CaptchaHelper captha = new Helpers.CaptchaHelper(HttpContext);
            if (captha.Validate())
            {
                if (listedeGorunsun==null)
                {
                    listedeGorunsun = "0";
                }
                bool resmiKaydet = false;
                if (resim != null)
                {
                    if (resim.ContentLength > 0)
                    {
                        double bayt = resim.ContentLength;
                        double mbBirim = 1048576;
                        double megabayt = bayt / mbBirim;
                        if (megabayt>3)
                        {
                            ViewBag.StartupScript = "alert('Takım resmi (max) 3mb olmalıdır!');";
                        }
                        else
                        {
                            resmiKaydet = true;
                        }
                    }
                }

                var kontrol = vt.tblKullanici.Where(b => b.telefonNo == telefonNo).FirstOrDefault();
                if (kontrol!=null)
                {
                    ViewBag.StartupScript = "alert('Bu telefon numarası başka bir hesaba tanımmlı.');";
                    return View("Index");
                }
                

                tblKullanici t = new tblKullanici();
                t.adSoyad = adSoyad;
                t.sifre = sifre;
                t.telefonNo = telefonNo;
                t.aktif = 1;
                t.admin = 0;
                t.yetkiliMail = mailadresi;
               // t.takimAdi = takimAdi;
                //t.takimYetkilisi = yetkiliAdi;
               // t.yetkiliTelNo = yetkiliTelNo;
                //t.takimAciklama = aciklama;
                //t.takimDurumu = int.Parse(TakimDurumlari);
                //t.il = int.Parse(Sehirler);
                //t.ilce = int.Parse(Ilceler);
                //t.gorunsunMu = listedeGorunsun.Contains("on") ? 1 : 0;
                if (!resmiKaydet)
                {
                    t.takimLogo = "noimage.png";
                }

                var takim= vt.tblKullanici.Add(t);
                vt.SaveChanges();

                if (resmiKaydet)
                {
                    string dosyaAdi = takim.kullaniciId.ToString();
                    string filePath = Path.Combine(Server.MapPath("~/img"), dosyaAdi + Path.GetExtension(resim.FileName));
                    resim.SaveAs(filePath);

                    vt.tblKullanici.Attach(takim);
                    var entry = vt.Entry(takim);
                    takim.takimLogo = dosyaAdi + Path.GetExtension(resim.FileName);
                    entry.Property(e => e.takimLogo).IsModified = true;
                     
                    vt.SaveChanges();
                } 


                ViewBag.StartupScript = "alert('Üyelik işleminiz tamamlandı.');";
                Session["golliglogin"] = takim.kullaniciId;
                Session.Add("golliglogintip", "100");

                mailGonder(adSoyad, mailadresi);

                try
                {
                    return RedirectToAction("index", "sahadetay", new { sahaAdi = Session["back"], Tarih = Session["backtarih"], kod = Session["backkod"] });
                }
                catch (Exception)
                { return Redirect("/"); }

                //return Redirect("/");
            }
            else
            {
                ViewBag.StartupScript = "alert('Doğrulama işlemi başarısız!');";
            }
             
            return View("Index");
        }

        private void mailGonder(string adSoyad, string mailadresi)
        {
            try
            { 
                var x = vt.tblSiteBilgileri.FirstOrDefault();
                if (x != null)
                {
                    var body = "";
                    var message = new MailMessage();
                    message.To.Add(new MailAddress(x.mail));
                    message.From = new MailAddress("info@gollig.com");
                      
                        body = "<p>Merhaba, " + adSoyad + " ismi ile yeni bir kullanıcımız var.</p><p>Mail Adresi: " + mailadresi +  "</p><p>www.gollig.com</p>";
                        message.Subject = "GolLig | Yeni Bir Üye!";
                    


                    message.Body = (body);
                    message.IsBodyHtml = true;

                    NetworkCredential netCred = new NetworkCredential("info@gollig.com", "GolLig2016");

                    SmtpClient smtpobj = new SmtpClient("gollig.com");
                    smtpobj.EnableSsl = false;
                    smtpobj.Credentials = netCred;
                    message.IsBodyHtml = true;


                    smtpobj.Send(message);

                    // return Json(new { basarili = true, text = "Şifreniz mail adresinize gönderildi." });
                } 
            }
            catch (Exception ex)
            {
                //return Json(new { basarili = false, text = "İşlem gerçekleşirken beklenmedik bir hata oluştu.Lütfen tekrar deneyin." });
            }
        }

        [HttpPost]
        public JsonResult sifremiunuttum(string telefon)
        {
            try
            { 
                var x = vt.tblKullanici.Where(z => z.telefonNo == telefon).FirstOrDefault();
                if (x != null)
                {
                    var body = "<p>Şifreniz: " + x.sifre + " </p><p>www.gollig.com</p>";
                    var message = new MailMessage();
                    message.To.Add(new MailAddress(x.yetkiliMail));
                    message.From = new MailAddress("info@gollig.com");
                    message.Subject = "GolLig | Şifremi Unuttum";
                    message.Body = (body);
                    message.IsBodyHtml = true;

                    NetworkCredential netCred = new NetworkCredential("info@gollig.com", "GolLig2016");

                    SmtpClient smtpobj = new SmtpClient("gollig.com");
                    smtpobj.EnableSsl = false;
                    smtpobj.Credentials = netCred;
                    message.IsBodyHtml = true;


                    smtpobj.Send(message);

                    return Json( new { basarili=true,text="Şifreniz mail adresinize gönderildi." } );
                }
                return Json(new { basarili = false, text = "Bu telefon numarasına bağlı bir hesap bulunamadı." });
            }
            catch (Exception ex)
            {
                return Json(new { basarili = false, text = "İşlem gerçekleşirken beklenmedik bir hata oluştu.Lütfen tekrar deneyin." });
            }
        }





    }
}

