﻿using MvcGolLig.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcGolLig.Controllers
{
    public class SahalarController : Controller
    {
        vtGolLigEntities vt = new vtGolLigEntities();
        // GET: Sahalar
        public ActionResult Index(string Sehirler, string Ilceler)
        {
            sehirleriYukle();
           

            try
            {
                int pSehir = int.Parse(Sehirler);
                int pIlce = int.Parse(Ilceler);
                IlcerleriGetir(pSehir);

                if (pSehir==-1 && pIlce==-1)
                {
                    return View(vt.tblSaha.Where(x=>x.aktif==1).ToList());
                }
                else if (pSehir!=-1 && pIlce==-1)
                {
                    return View(vt.tblSaha.Where(x => x.aktif == 1 && x.il==pSehir).ToList());
                }
                else if (pSehir != -1 && pIlce != -1)
                {
                    return View(vt.tblSaha.Where(x => x.aktif == 1 && x.il == pSehir && x.ilce==pIlce).ToList());
                }
                else
                {
                    return View(vt.tblSaha.Where(x => x.aktif == 1).ToList());
                }
            }
            catch (Exception)
            {
                IlcerleriGetir(0);
                return View(vt.tblSaha.Where(x => x.aktif == 1).ToList());
            }

            return View(vt.tblSaha.Where(x => x.aktif == 1).ToList());
        }

        void sehirleriYukle()
        {
            List<SelectListItem> sehirList = new List<SelectListItem>();
            sehirList.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });
            foreach (var item in vt.iller.ToList().OrderBy(x => x.sehir))
            {
                sehirList.Add(new SelectListItem { Text = item.sehir, Value = item.id.ToString() });
            }
            ViewBag.Sehirler = sehirList;
        }

        public ActionResult IlcerleriGetir(int id = 0)
        {
            List<SelectListItem> l = new List<SelectListItem>();
            l.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });

            foreach (var item in vt.ilceler.Where(x => x.sehir == id).ToList().OrderBy(x=>x.ilce) )
            {
                l.Add(new SelectListItem { Text = item.ilce, Value = item.id.ToString() });
            }

            ViewBag.Ilceler = l;
            return View();
        }


    }
}