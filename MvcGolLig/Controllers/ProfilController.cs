﻿using MvcGolLig.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcGolLig.Controllers
{
    public class ProfilController : Controller
    {
        vtGolLigEntities vt = new vtGolLigEntities();
        // GET: Profil
        public ActionResult Index(string adsoyad,string sifre, string telefonno,string takimAdi,
            string takimAciklama,string takimDurumu,string yetkiliMail,
            string il,string Ilceler="-1" ,string gorunsun="-1", HttpPostedFileBase resim=null)
        {
            try
            {
                if (telefonno!=null)
                {
                    telefonno = telefonno.Replace("(", "");
                    telefonno = telefonno.Replace(")", "");
                    telefonno = telefonno.Replace(" ", "");
                    telefonno = telefonno.Replace("-", "");
                }

                takimDurumlariYukle();
                sehirleriYukle();
                IlcerleriGetir(0);

                if (Session["golliglogin"]!=null)
                {
                    int id = int.Parse(Session["golliglogin"].ToString());
                    tblKullanici takim= vt.tblKullanici.Where(x => x.kullaniciId == id).FirstOrDefault();

                    rezleriGetir(id);
                    if (takim.il!=null)
                    {
                        IlcerleriGetir(takim.il.Value);
                    }
                     

                    if (takim!=null && adsoyad!=null)
                    {
                        if (gorunsun == null)
                        {
                            gorunsun = "0";
                        }

                        bool resmiKaydet = false;
                        if (resim != null)
                        {
                            if (resim.ContentLength > 0)
                            {
                                double bayt = resim.ContentLength;
                                double mbBirim = 1048576;
                                double megabayt = bayt / mbBirim;
                                if (megabayt > 3)
                                {
                                    ViewBag.StartupScript = "alert('Takım resmi (max) 3mb olmalıdır!');";
                                }
                                else
                                {
                                    resmiKaydet = true;
                                }
                            }
                        }


                        takim.adSoyad = adsoyad;
                        takim.sifre = sifre;
                        takim.telefonNo = telefonno;
                        takim.takimAdi = takimAdi;
                        takim.yetkiliMail = yetkiliMail;
                        //takim.takimYetkilisi = takimYetkilisi;
                        //takim.telefonNo = yetkiliTelNo;
                        takim.takimAciklama = takimAciklama;
                        takim.takimDurumu = int.Parse(takimDurumu);
                        takim.il = int.Parse(il);
                        takim.ilce = int.Parse(Ilceler);
                        takim.gorunsunMu = gorunsun.Contains("on") ? 1 : 0;

                        vt.SaveChanges();

                        if (resmiKaydet)
                        {
                            string dosyaAdi = takim.kullaniciId.ToString();
                            //string filePath = Path.Combine(Server.MapPath("~/img"), dosyaAdi + Path.GetExtension(resim.FileName));
                            //resim.SaveAs(filePath);
                            string targetFolder = Server.MapPath("~/img");
                            string targetPath = Path.Combine(targetFolder, dosyaAdi + Path.GetExtension(resim.FileName));
                            resim.SaveAs(targetPath);



                            vt.tblKullanici.Attach(takim);
                            var entry = vt.Entry(takim);
                            takim.takimLogo = dosyaAdi + Path.GetExtension(resim.FileName);
                            entry.Property(e => e.takimLogo).IsModified = true;

                            vt.SaveChanges();
                        }
                        ViewBag.StartupScript = "alert('Bilgileriniz güncellendi!');";
                        return View(takim);
                    }
                    else if (takim!=null)
                    {
                        return View(takim);
                    }
                    else
                    {
                        return Redirect("/");
                    }
                }
                else
                {
                    return Redirect("/");
                }
            }
            catch (Exception)
            {
                return Redirect("/");
            }
            
            return View(vt.tblKullanici.FirstOrDefault());
        }

        private void rezleriGetir(int id=-1)
        {
            ViewBag.Rezler= vt.tblRezervasyon.Where(x => x.takim == id).ToList();

           // ViewBag.Rezler = vt.tblRezervasyon.ToList();
        }

        private void takimDurumlariYukle()
        {/*
            List<SelectListItem> takimDurumList = new List<SelectListItem>();
            //takimDurumList.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });
            foreach (var item in vt.tblTakimDurumu.ToList())
            {
                takimDurumList.Add(new SelectListItem { Text = item.aciklama, Value = item.takimDurumuId.ToString() });
            }
            ViewBag.TakimDurumlari = takimDurumList;
            */
            ViewBag.TakimDurumlari = new SelectList(vt.tblTakimDurumu.ToList(), "takimDurumuId", "aciklama");
        }
        void sehirleriYukle()
        {/*
            List<SelectListItem> sehirList = new List<SelectListItem>();
            //sehirList.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });
            foreach (var item in vt.iller.ToList())
            {
                sehirList.Add(new SelectListItem { Text = item.sehir, Value = item.id.ToString() });
            }
            //ViewBag.Sehirler = sehirList;
            */
            ViewBag.Sehirler = new SelectList(vt.iller.ToList(), "id", "sehir");
        }

        public ActionResult IlcerleriGetir(int id = 0)
        {
            List<SelectListItem> l = new List<SelectListItem>();
            l.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });
            foreach (var item in vt.ilceler.Where(x => x.sehir == id).ToList())
            {
                l.Add(new SelectListItem { Text = item.ilce, Value = item.id.ToString() });
            }
            ViewBag.Ilceler = l;
            
            return View();
        }

        public JsonResult iptalEt(string id="-1")
        {
            try
            {
                if (Session["golliglogin"]!=null)
                {
                    int pRezId = int.Parse(id);
                    int pTakim = int.Parse(Session["golliglogin"].ToString());

                    var rez=vt.tblRezervasyon.Where(x => x.rezId == pRezId && x.takim == pTakim).FirstOrDefault();
                    if (rez!=null)
                    {
                        rez.durum = -1;
                        vt.SaveChanges();
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("Rezervasyon kodu geçersiz.", JsonRequestBehavior.AllowGet);
                    } 
                }
                else
                {
                    return Json("Lütfen giriş yapınız.", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("Lütfen giriş yapınız.", JsonRequestBehavior.AllowGet);
            } 
        }






   }
}