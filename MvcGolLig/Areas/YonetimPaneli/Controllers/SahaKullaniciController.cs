﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcGolLig.Models;

namespace MvcGolLig.Areas.YonetimPaneli.Controllers
{
    public class SahaKullaniciController : Controller
    {
        private vtGolLigEntities db = new vtGolLigEntities();

        // GET: YonetimPaneli/SahaKullanici
        public ActionResult Index()
        {
            var tblSahaKullanici = db.tblSahaKullanici.Include(t => t.tblKullanici).Include(t => t.tblSaha);
            return View(tblSahaKullanici.ToList());
        }

        // GET: YonetimPaneli/SahaKullanici/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblSahaKullanici tblSahaKullanici = db.tblSahaKullanici.Find(id);
            if (tblSahaKullanici == null)
            {
                return HttpNotFound();
            }
            return View(tblSahaKullanici);
        }

        // GET: YonetimPaneli/SahaKullanici/Create
        public ActionResult Create()
        {
            ViewBag.panelKullanici = new SelectList(db.tblKullanici, "kullaniciId", "adSoyad");
            ViewBag.saha = new SelectList(db.tblSaha, "sahaId", "adi");
            return View();
        }

        // POST: YonetimPaneli/SahaKullanici/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "sahaSahibiId,saha,panelKullanici")] tblSahaKullanici tblSahaKullanici)
        {
            if (ModelState.IsValid)
            {
                db.tblSahaKullanici.Add(tblSahaKullanici);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.panelKullanici = new SelectList(db.tblKullanici, "kullaniciId", "kullaniciAdi", tblSahaKullanici.panelKullanici);
            ViewBag.saha = new SelectList(db.tblSaha, "sahaId", "adi", tblSahaKullanici.saha);
            return View(tblSahaKullanici);
        }

        // GET: YonetimPaneli/SahaKullanici/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblSahaKullanici tblSahaKullanici = db.tblSahaKullanici.Find(id);
            if (tblSahaKullanici == null)
            {
                return HttpNotFound();
            }
            ViewBag.panelKullanici = new SelectList(db.tblKullanici, "kullaniciId", "kullaniciAdi", tblSahaKullanici.panelKullanici);
            ViewBag.saha = new SelectList(db.tblSaha, "sahaId", "adi", tblSahaKullanici.saha);
            return View(tblSahaKullanici);
        }

        // POST: YonetimPaneli/SahaKullanici/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "sahaSahibiId,saha,panelKullanici")] tblSahaKullanici tblSahaKullanici)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblSahaKullanici).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.panelKullanici = new SelectList(db.tblKullanici, "kullaniciId", "kullaniciAdi", tblSahaKullanici.panelKullanici);
            ViewBag.saha = new SelectList(db.tblSaha, "sahaId", "adi", tblSahaKullanici.saha);
            return View(tblSahaKullanici);
        }

        // GET: YonetimPaneli/SahaKullanici/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblSahaKullanici tblSahaKullanici = db.tblSahaKullanici.Find(id);
            if (tblSahaKullanici == null)
            {
                return HttpNotFound();
            }
            return View(tblSahaKullanici);
        }

        // POST: YonetimPaneli/SahaKullanici/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblSahaKullanici tblSahaKullanici = db.tblSahaKullanici.Find(id);
            db.tblSahaKullanici.Remove(tblSahaKullanici);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
