﻿using MvcGolLig.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcGolLig.Areas.YonetimPaneli.Controllers
{
    public class TakimSeviyeleriController : Controller
    {
        vtGolLigEntities vt = new vtGolLigEntities();
        // GET: YonetimPaneli/TakimSeviyeleri
        public ActionResult Index()
        {
            return View(vt.tblTakimDurumu.ToList());
        }

        public ActionResult YeniKayit(string takimseviyesi)
        {
            tblTakimDurumu td = new tblTakimDurumu();
            td.aciklama = takimseviyesi;
            vt.tblTakimDurumu.Add(td);
            vt.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Sil(int id=0)
        {
            tblTakimDurumu td = vt.tblTakimDurumu.Where(x => x.takimDurumuId == id).FirstOrDefault();
            if (td!=null)
            {
                vt.tblTakimDurumu.Remove(td);
                vt.SaveChanges();
            }  
             
            return RedirectToAction("Index");
        }

    }
}