﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcGolLig.Models;

namespace MvcGolLig.Areas.YonetimPaneli.Controllers
{
    public class SahaTipiController : Controller
    {
        private vtGolLigEntities db = new vtGolLigEntities();

        // GET: YonetimPaneli/SahaTipi
        public ActionResult Index()
        {
            return View(db.tblSahaTipi.ToList());
        }

        // GET: YonetimPaneli/SahaTipi/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblSahaTipi tblSahaTipi = db.tblSahaTipi.Find(id);
            if (tblSahaTipi == null)
            {
                return HttpNotFound();
            }
            return View(tblSahaTipi);
        }

        // GET: YonetimPaneli/SahaTipi/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: YonetimPaneli/SahaTipi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "sahaTipId,aciklama")] tblSahaTipi tblSahaTipi)
        {
            if (ModelState.IsValid)
            {
                db.tblSahaTipi.Add(tblSahaTipi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblSahaTipi);
        }

        // GET: YonetimPaneli/SahaTipi/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblSahaTipi tblSahaTipi = db.tblSahaTipi.Find(id);
            if (tblSahaTipi == null)
            {
                return HttpNotFound();
            }
            return View(tblSahaTipi);
        }

        // POST: YonetimPaneli/SahaTipi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "sahaTipId,aciklama")] tblSahaTipi tblSahaTipi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblSahaTipi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblSahaTipi);
        }

        // GET: YonetimPaneli/SahaTipi/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblSahaTipi tblSahaTipi = db.tblSahaTipi.Find(id);
            if (tblSahaTipi == null)
            {
                return HttpNotFound();
            }
            return View(tblSahaTipi);
        }

        // POST: YonetimPaneli/SahaTipi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblSahaTipi tblSahaTipi = db.tblSahaTipi.Find(id);
            db.tblSahaTipi.Remove(tblSahaTipi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
