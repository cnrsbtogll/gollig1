﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcGolLig.Models;

namespace MvcGolLig.Areas.YonetimPaneli.Controllers
{
    public class KullaniciAyarlariController : Controller
    {
        private vtGolLigEntities db = new vtGolLigEntities();
         
        public ActionResult Index()
        {
            var tblKullanici = db.tblKullanici.Include(t => t.ilceler).Include(t => t.iller).Include(t => t.tblTakimDurumu);

            ViewBag.Aktifler = db.tblKullanici.Where(x => x.aktif == 1).ToList().OrderBy(x=>x.adSoyad);
            ViewBag.Pasifler = db.tblKullanici.Where(x => x.aktif != 1).ToList().OrderBy(x => x.adSoyad);

            return View(tblKullanici.ToList().OrderByDescending(c=>c.kullaniciId));
        }
         
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblKullanici tblKullanici = db.tblKullanici.Find(id);
            if (tblKullanici == null)
            {
                return HttpNotFound();
            }
            return View(tblKullanici);
        }
         
        public ActionResult Create()
        {
            ViewBag.ilce = new SelectList(db.ilceler, "id", "ilce");
            ViewBag.il = new SelectList(db.iller, "id", "sehir");
            ViewBag.takimDurumu = new SelectList(db.tblTakimDurumu, "takimDurumuId", "aciklama");
            return View();
        }
         
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "kullaniciId,adSoyad,sifre,telefonNo,takimAdi,yetkiliMail,takimDurumu,takimAciklama,takimLogo,gorunsunMu,il,ilce,admin,aktif")] tblKullanici tblKullanici)
        {
            if (ModelState.IsValid)
            {
                db.tblKullanici.Add(tblKullanici);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ilce = new SelectList(db.ilceler, "id", "ilce", tblKullanici.ilce);
            ViewBag.il = new SelectList(db.iller, "id", "sehir", tblKullanici.il);
            ViewBag.takimDurumu = new SelectList(db.tblTakimDurumu, "takimDurumuId", "aciklama", tblKullanici.takimDurumu);
            return View(tblKullanici);
        }
         
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblKullanici tblKullanici = db.tblKullanici.Find(id);
            if (tblKullanici == null)
            {
                return HttpNotFound();
            }
            ViewBag.ilce = new SelectList(db.ilceler, "id", "ilce", tblKullanici.ilce);
            ViewBag.il = new SelectList(db.iller, "id", "sehir", tblKullanici.il);
            ViewBag.takimDurumu = new SelectList(db.tblTakimDurumu, "takimDurumuId", "aciklama", tblKullanici.takimDurumu);
            return View(tblKullanici);
        }
         
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "kullaniciId,adSoyad,sifre,telefonNo,takimAdi,yetkiliMail,takimDurumu,takimAciklama,takimLogo,gorunsunMu,il,ilce,admin,aktif")] tblKullanici tblKullanici)
        {
            if (tblKullanici.telefonNo!=null)
            {
                tblKullanici.telefonNo = tblKullanici.telefonNo.Replace("(", "");
                tblKullanici.telefonNo = tblKullanici.telefonNo.Replace(")", "");
                tblKullanici.telefonNo = tblKullanici.telefonNo.Replace(" ", "");
                tblKullanici.telefonNo = tblKullanici.telefonNo.Replace("-", "");
            }
            if (ModelState.IsValid)
            {
                db.Entry(tblKullanici).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ilce = new SelectList(db.ilceler, "id", "ilce", tblKullanici.ilce);
            ViewBag.il = new SelectList(db.iller, "id", "sehir", tblKullanici.il);
            ViewBag.takimDurumu = new SelectList(db.tblTakimDurumu, "takimDurumuId", "aciklama", tblKullanici.takimDurumu);
            return View(tblKullanici);
        }
         
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblKullanici tblKullanici = db.tblKullanici.Find(id);
            if (tblKullanici == null)
            {
                return HttpNotFound();
            }
            return View(tblKullanici);
        }
         
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblKullanici tblKullanici = db.tblKullanici.Find(id);
            db.tblKullanici.Remove(tblKullanici);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult IlcerleriGetir(int id = 1)
        {
            List<SelectListItem> l = new List<SelectListItem>();
            l.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });

            foreach (var item in db.ilceler.Where(x => x.sehir == id).ToList())
            {
                l.Add(new SelectListItem { Text = item.ilce, Value = item.id.ToString() });
            }

            ViewBag.ilce = l;
            return View();
        }

        [HttpPost]
        public JsonResult DurumDegistir(int id, int durum = 1)
        {
            try
            {
                tblKullanici rez = db.tblKullanici.Where(d => d.kullaniciId == id).FirstOrDefault();
                if (rez != null)
                {
                    rez.aktif = durum;
                    db.SaveChanges();
                    if (durum == 1)
                    {  
                        return Json(new { basarili = true, text = "Kullanıcı aktifleştirildi." }); 
                    }
                    else
                    {
                        return Json(new { basarili = true, text = "Kullanıcı engellendi." });
                    }
                }
                else
                {
                    return Json(new { basarili = false, text = "Kullanıcı güncelleme sırasında hata oluştu." });
                }
            }
            catch (Exception)
            {
                return Json(new { basarili = false, text = "Kullanıcı güncelleme sırasında hata oluştu." });
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
