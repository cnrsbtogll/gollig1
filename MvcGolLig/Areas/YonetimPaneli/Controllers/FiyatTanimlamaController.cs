﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcGolLig.Models;

namespace MvcGolLig.Areas.YonetimPaneli.Controllers
{
    public class FiyatTanimlamaController : Controller
    {
        private vtGolLigEntities db = new vtGolLigEntities();

        // GET: YonetimPaneli/FiyatTanimlama
        public ActionResult Index(int saha=-1)
        {
            try
            {
                int id = int.Parse(Session["golliglogin"].ToString());
                sahaYukle(id);

                //giriş yapan kullanıcının yetkisi olan sahalar
                if (saha==-1)
                {
                    var sahalar = db.tblSahaKullanici.Where(c => c.panelKullanici == id).Select(c => c.saha);

                    var tblFiyat = db.tblFiyat.Where(x => sahalar.Contains(x.saha)).Include(t => t.tblRezervasyonSaatleri).Include(t => t.tblSaha);

                    return View(tblFiyat.ToList().OrderByDescending(c => c.saha));
                }
                else
                {
                    var sahalar = db.tblSahaKullanici.Where(c => c.panelKullanici == id && c.saha.Value==saha).Select(c => c.saha);

                    var tblFiyat = db.tblFiyat.Where(x => sahalar.Contains(x.saha)).Include(t => t.tblRezervasyonSaatleri).Include(t => t.tblSaha);

                    return View(tblFiyat.ToList().OrderByDescending(c => c.saha));
                }
            }
            catch (Exception)
            {
                return Redirect("/yonetimpaneli");
            }
             
        }
        void sahaYukle(int kulid)
        {
            var sahalar = db.tblSahaKullanici.Where(c => c.panelKullanici == kulid).Select(c => c.saha );

            var sahaList = db.tblSaha.Where(x => sahalar.Contains(x.sahaId)).ToList();


            List<SelectListItem> l = new List<SelectListItem>();
            foreach (var item in sahaList)
            {
                l.Add(new SelectListItem { Text = item.adi, Value = item.sahaId.ToString() });
            }

            ViewBag.SahaList = l;
        }

        [HttpPost]
        public JsonResult TumunuGuncelle(int saha=0, double fiyat = 0)
        {
            try
            {
                int kulid = int.Parse(Session["golliglogin"].ToString());

                tblSahaKullanici yetki = db.tblSahaKullanici.Where(d => d.panelKullanici == kulid && d.saha==saha).FirstOrDefault();
                if (yetki != null)
                {

                    foreach (var item in db.tblFiyat.Where(d=>d.saha==saha).ToList())
                    {
                        item.fiyat = fiyat;
                    }
                    db.SaveChanges();
                    return Json(new { basarili = true, text = "Fiyatlar güncellendi." });                    
                }
                else
                {
                    return Json(new { basarili = false, text = "Saha güncelleme yetkisine sahip değilsiniz." });
                }
            }
            catch (Exception)
            {
                return Json(new { basarili = false, text = "Fiyat güncelleme sırasında hata oluştu." });
            }
        }





        // GET: YonetimPaneli/FiyatTanimlama/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblFiyat tblFiyat = db.tblFiyat.Find(id);
            if (tblFiyat == null)
            {
                return HttpNotFound();
            }
            ViewBag.rezSaati = new SelectList(db.tblRezervasyonSaatleri, "rezId", "aciklama", tblFiyat.rezSaati);
            ViewBag.saha = new SelectList(db.tblSaha, "sahaId", "adi", tblFiyat.saha);
            return View(tblFiyat);
        }
         
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "fiyatId,saha,rezSaati,fiyat")] tblFiyat tblFiyat)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblFiyat).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.rezSaati = new SelectList(db.tblRezervasyonSaatleri, "rezId", "aciklama", tblFiyat.rezSaati);
            ViewBag.saha = new SelectList(db.tblSaha, "sahaId", "adi", tblFiyat.saha);
            return View(tblFiyat);
        }

        [HttpPost]
        public JsonResult fiyatguncelle(int id,int yeniFiyat)
        {
            try
            {
                var fiyat= db.tblFiyat.Where(x => x.fiyatId == id).FirstOrDefault();
                if (fiyat!=null)
                {
                    fiyat.fiyat = yeniFiyat;
                    db.Entry(fiyat).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            } 
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
