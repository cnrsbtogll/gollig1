//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MvcGolLig.Models
{
    using System;
    
    public partial class fnc_sahaBazliRezFiyat_Result
    {
        public int rezId { get; set; }
        public string aciklama { get; set; }
        public Nullable<double> fiyat { get; set; }
        public Nullable<int> takimId { get; set; }
        public Nullable<int> rezDurum { get; set; }
    }
}
