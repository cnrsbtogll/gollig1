﻿using System.Web.Mvc;

namespace MvcGolLig.Areas.YonetimPaneli
{
    public class YonetimPaneliAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "YonetimPaneli";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "YonetimPaneli_default",
                "YonetimPaneli/{controller}/{action}/{id}",
                new { controller="Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}