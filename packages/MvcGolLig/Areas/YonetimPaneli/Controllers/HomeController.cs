﻿using MvcGolLig.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace MvcGolLig.Areas.YonetimPaneli.Controllers
{
    public class HomeController : Controller
    {
        vtGolLigEntities vt = new vtGolLigEntities();
        // GET: YonetimPaneli/Home
        public ActionResult Index(int id = 0, string tarih = "")
        { 
            
            sayilariYaz();
            ViewBag.bugun = DateTime.Now.ToString("dd.MM.yyyy");

            try
            {
                if (tarih!="")
                {
                    sahaRezRapor(int.Parse(Session["golliglogin"].ToString()), tarih);
                    ViewBag.tarihgirisi = tarih;
                }
                else
                {
                    sahaRezRapor(int.Parse(Session["golliglogin"].ToString()), DateTime.Now.ToString("dd.MM.yyyy"));
                }
                
            }
            catch (Exception)
            {
                Redirect("/");
            }

            ViewBag.takim = new SelectList(vt.tblKullanici.Where(x=>x.aktif==1), "telefonNo", "adSoyad");
            return View();
        }

        void sahaRezRapor(int id=0,string tarih="")
        {
            try
            {
                if (id==0)
                {
                    id= int.Parse(Session["golliglogin"].ToString()); 
                }
                if (tarih=="")
                {
                    tarih = DateTime.Now.ToString("dd.MM.yyyy");
                }
            }
            catch (Exception)
            {
                Redirect("/");
            }
            var sConnection = ((SqlConnection)vt.Database.Connection);

           
            if (sConnection != null && sConnection.State == ConnectionState.Closed)
            {
                sConnection.Open();
            }
            List<Rapor> raporList = new List<Rapor>();
            DateTime tarihBas = DateTime.Now;
            try
            {
                tarihBas = DateTime.Parse(tarih);
            }
            catch (Exception)
            { }

            using (SqlDataAdapter ad = new SqlDataAdapter())
            {
                //SqlDataAdapter com = new SqlDataAdapter("exec sp_sahaRezRapor " + id + ", '" + tarih + "' ", sConnection);
                //com.Fill(dt);

                var sahalar=vt.tblSahaKullanici.Where(x => x.panelKullanici == id).ToList().OrderBy(x=>x.tblSaha.adi);

                foreach (var item in sahalar.ToList())
                {
                    Rapor r = new Rapor();
                    r.SahaAdi = item.tblSaha.adi;

                    DataTable dt = new DataTable();
                    SqlDataAdapter com = new SqlDataAdapter("exec sp_SonRezervasyonlar '"+item.saha+"','"+tarihBas.ToShortDateString()+"' "+
                        ",'"+tarihBas.AddDays(1).ToShortDateString()+"' "+
                        ",'" + tarihBas.AddDays(2).ToShortDateString() + "' "+
                        ",'" + tarihBas.AddDays(3).ToShortDateString() + "', "+
                        "'" + tarihBas.AddDays(4).ToShortDateString() + "' "+
                        ",'" + tarihBas.AddDays(5).ToShortDateString() + "' "+
                        ",'" + tarihBas.AddDays(6).ToShortDateString() + "'  ", sConnection);

                    com.Fill(dt);

                    r.Rezler = dt;
                    raporList.Add(r);
                }

                
            }
            sConnection.Close();

            ViewBag.Rezler = raporList;
        }

        void sayilariYaz()
        {
            var saha = vt.vw_publicsaha.ToList();
            var takim = vt.vw_publictakim.ToList();
            var mac = vt.vw_publicmac.ToList();
            var kullanici = vt.publickullanici.ToList();


            ViewBag.KayitliSaha = saha.Count();
            ViewBag.KayitliTakim = takim.Count();
            ViewBag.KayitliMac = mac.Count();
            ViewBag.KayitliKullanici = kullanici.Count();


            ViewBag.publicsaha = saha;
            ViewBag.publictakim = takim;
            ViewBag.publickullanici = kullanici;
            ViewBag.publicmac = mac.OrderByDescending(x=>x.tarih) ;
        }

        public ActionResult cikis()
        {
            Session.Abandon();
            return RedirectToAction("Index");// Redirect("/");
        }

        [HttpPost]
        public JsonResult rezervasyonYap(int sahaId,string takim,string tarih,string takimTel,string saat,double fiyat,int fiyatId)
        {
            try
            {
                if (takimTel!=null)
                {
                    takimTel = takimTel.Replace("(", "");
                    takimTel = takimTel.Replace(")", "");
                    takimTel = takimTel.Replace("-", "");
                    takimTel = takimTel.Replace(" ", "");
                }
                DateTime dt = DateTime.Parse(tarih);
                var kul=vt.tblKullanici.Where(x => x.telefonNo == takimTel).FirstOrDefault();
                var tFiyat = vt.tblFiyat.Where(x => x.fiyatId == fiyatId).FirstOrDefault();

                tblRezervasyon rez = new tblRezervasyon();
                rez.saha = sahaId;
                rez.tarih = dt;
                rez.durum = 1;

                if (kul!=null)
                {
                    rez.tblKullanici = kul;
                    rez.takim = kul.kullaniciId;
                }
                else
                {
                    rez.aciklama = takim + " " + takimTel;
                }
                if (tFiyat != null)
                {
                    rez.fiyat = fiyat;
                    rez.rezSaati = tFiyat.rezSaati;
                    rez.tblFiyat = tFiyat;
                }
                else
                {
                    return Json(new { basarili = false, text = "Rezervasyon oluşturma sırasında hata oluştu.Fiyat bilgilerine erişilemedi." });
                }

                vt.tblRezervasyon.Add(rez);
                vt.SaveChanges();

                return Json(new { basarili = true, text = "Rezervasyon işlemi yapıldı." });
            }
            catch (Exception)
            {
                return Json(new { basarili = false, text = "Rezervasyon oluşturma sırasında hata oluştu." });
            }
        }

        [HttpPost]
        public JsonResult DurumDegistir(int id, int durum = 1)
        {
            try
            {
                tblRezervasyon rez = vt.tblRezervasyon.Where(d => d.rezId == id).FirstOrDefault();
                int userid = int.Parse(Session["golliglogin"].ToString());
                var yetki = vt.tblSahaKullanici.Where(x => x.saha == rez.saha && x.panelKullanici == userid).FirstOrDefault();
                if (yetki == null)
                {
                    return Json(new { basarili = false, text = "Bu rezervasyonu güncelleme yetkiniz yok." });
                }

                if (rez != null)
                {
                    rez.durum = durum;
                    vt.SaveChanges();
                    var x = rez.tblKullanici;
                    if (durum == 1)
                    { 
                        rezervasyonSahibineMailGonder(durum, x, rez); 
                        return Json(new { basarili = true, text = "Rezervasyon onaylandı." }); 
                    }
                    else
                    {
                        rezervasyonSahibineMailGonder(durum, x, rez);
                        return Json(new { basarili = true, text = "Rezervasyon iptal edildi." });
                    }
                }
                else
                {
                    return Json(new { basarili = false, text = "Rezervasyon güncelleme sırasında hata oluştu." });
                }
            }
            catch (Exception)
            {
                return Json(new { basarili = false, text = "Rezervasyon güncelleme sırasında hata oluştu." });
            }
        }

        private void rezervasyonSahibineMailGonder(int durum, tblKullanici x, tblRezervasyon rez)
        {
            try
            {
                //var x = vt.tblKullanici.Where(z => z.telefonNo == telefon).FirstOrDefault();
                //var x = db.tblSiteBilgileri.FirstOrDefault();
                if (x != null)
                {
                    var body = "";
                    var message = new MailMessage();
                    message.To.Add(new MailAddress(x.yetkiliMail));
                    message.From = new MailAddress("info@gollig.com");
                    if (durum == 1)
                    {
                        body = "<p>Merhaba " + x.adSoyad + ",</p><p>" + rez.tblSaha.adi + " isimli halı sahasına yaptığın " + rez.tarih.Value.ToString("dd.MM.yyyy") + " " + rez.tblRezervasyonSaatleri.aciklama + " tarihli rezervasyonun onaylanmıştır.</p>" +
                       "<p>www.gollig.com</p>";
                        message.Subject = "GolLig | Rezervasyonun Onaylandı!";
                    }
                    else
                    {
                        body = "<p>Merhaba " + x.adSoyad + ",</p><p>" + rez.tblSaha.adi + " isimli halı sahasına yaptığın " + rez.tarih.Value.ToString("dd.MM.yyyy") + " " + rez.tblRezervasyonSaatleri.aciklama + " tarihli rezervasyonun iptal edilmiştir." +
                            " </p>" +
                       "<p>www.gollig.com</p>";
                        message.Subject = "GolLig | Rezervasyonun Onaylanmadı!";
                    }


                    message.Body = (body);
                    message.IsBodyHtml = true;

                    NetworkCredential netCred = new NetworkCredential("info@gollig.com", "GolLig2016");

                    SmtpClient smtpobj = new SmtpClient("gollig.com");
                    smtpobj.EnableSsl = false;
                    smtpobj.Credentials = netCred;
                    message.IsBodyHtml = true;


                    smtpobj.Send(message);

                    // return Json(new { basarili = true, text = "Şifreniz mail adresinize gönderildi." });
                }
                // return Json(new { basarili = false, text = "Bu telefon numarasına bağlı bir hesap bulunamadı." });
            }
            catch (Exception ex)
            {
                //return Json(new { basarili = false, text = "İşlem gerçekleşirken beklenmedik bir hata oluştu.Lütfen tekrar deneyin." });
            }
        }



    } 

    public class Rapor
    {
        public string SahaAdi { get; set; }
        public DataTable Rezler { get; set; }
    }

}