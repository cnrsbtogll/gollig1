﻿using MvcGolLig.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace MvcGolLig.Areas.YonetimPaneli.Controllers
{
    public class LoginController : Controller
    {
        vtGolLigEntities db = new vtGolLigEntities();
        // GET: YonetimPaneli/Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult girisyap(string kullaniciAdi, string sifre)
        {
            var k=db.tblKullanici.Where(x => x.telefonNo == kullaniciAdi && x.sifre == sifre && x.aktif == 1).FirstOrDefault();
            if (k!=null)
            {
                Session.Add("golligadminlogin", k.kullaniciId);
                if (k.admin==1)
                {
                    Session.Add("golliglogintip", "0"); 
                }
                else
                {
                    Session.Add("golliglogintip", "100");
                }
                return Redirect("/yonetimpaneli/");
            }
            return RedirectToAction("Index");
        }


        [HttpPost]
        public ActionResult sifremiunuttum(string mail)
        {
            try
            {
                var x = db.tblKullanici.Where(z => z.yetkiliMail == mail).FirstOrDefault();
                if (x!=null)
                {
                    var body = "<p>Şifreniz: " + x.sifre + " </p><p>www.gollig.com</p>";
                    var message = new MailMessage();
                    message.To.Add(new MailAddress(mail));  
                    message.From = new MailAddress("info@gollig.com");  
                    message.Subject = "GolLig | Şifremi Unuttum";
                    message.Body =(body);
                    message.IsBodyHtml = true;

                    NetworkCredential netCred = new NetworkCredential("info@gollig.com", "GolLig2016");

                    SmtpClient smtpobj = new SmtpClient("gollig.com");
                    smtpobj.EnableSsl = false;
                    smtpobj.Credentials = netCred;
                    message.IsBodyHtml = true;


                    smtpobj.Send(message);
                }
                 
            }
            catch (Exception)
            {

            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult kayitol(string adsoyad,string telefon,string mail, string sifre)
        {
            try
            {
                var k = db.tblKullanici.Where(x => x.yetkiliMail == mail || x.telefonNo == telefon).FirstOrDefault();
                if (k!=null)
                {
                    // return Json("Bu bilgiler ile daha önce kayıt oluşturulmuş.Mail adresinizi ve kullanıcı adınızı kontrol edin.", JsonRequestBehavior.AllowGet);
                    return Json(new { Result = "Bu bilgiler ile daha önce kayıt oluşturulmuş.Mail adresinizi ve kullanıcı adınızı kontrol edin." });
                }


                tblKullanici pk = new tblKullanici();
                //pk.kullaniciAdi = kullaniciAdi;
                pk.sifre = sifre;
                pk.yetkiliMail = mail;
                pk.adSoyad = adsoyad;
                pk.telefonNo = telefon;
                //pk.tip = 1;
                pk.aktif = 1;

                db.tblKullanici.Add(pk);
                db.SaveChanges();
            }
            catch (Exception)
            { 
            }

            return Json(new { Result = true });
        }



    }
}