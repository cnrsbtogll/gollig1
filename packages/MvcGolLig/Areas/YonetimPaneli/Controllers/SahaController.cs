﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcGolLig.Models;
using System.IO;
using System.Net.Mail;

namespace MvcGolLig.Areas.YonetimPaneli.Controllers
{
    public class SahaController : Controller
    {
        private vtGolLigEntities db = new vtGolLigEntities();

        // GET: YonetimPaneli/Saha
        public ActionResult Index()
        {
            try
            {
                int id = int.Parse(Session["golliglogin"].ToString());
                int tip=int.Parse(Session["golliglogintip"].ToString());

                //giriş yapan kullanıcının yetkisi olan sahalar
                if (tip==100)
                {
                    var sahalar = db.tblSahaKullanici.Where(c => c.panelKullanici == id).Select(c => c.saha);

                    var tblSaha = db.tblSaha.Where(x => sahalar.Contains(x.sahaId)).Include(t => t.ilceler).Include(t => t.iller).Include(t => t.tblSahaTipi);
                    return View(tblSaha.ToList());
                }
                else
                {
                    var tblSaha = db.tblSaha.OrderByDescending(x=>x.sahaId).Include(t => t.ilceler).Include(t => t.iller).Include(t => t.tblSahaTipi);
                    return View(tblSaha.ToList());
                } 
            }
            catch (Exception)
            {
                return Redirect("/giris");
            } 
           
        }

        // GET: YonetimPaneli/Saha/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblSaha tblSaha = db.tblSaha.Find(id);
            if (tblSaha == null)
            {
                return HttpNotFound();
            }
            return View(tblSaha);
        }

        // GET: YonetimPaneli/Saha/Create
        public ActionResult Create()
        {
            //ViewBag.ilce = new SelectList(db.ilceler, "id", "ilce");
            ViewBag.il = new SelectList(db.iller, "id", "sehir");
            ViewBag.tipi = new SelectList(db.tblSahaTipi, "sahaTipId", "aciklama");

            IlcerleriGetir(0);

            return View();
        }

        public ActionResult IlcerleriGetir(int id = 0)
        {
            List<SelectListItem> l = new List<SelectListItem>();
            l.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });

            foreach (var item in db.ilceler.Where(x => x.sehir == id).ToList())
            {
                l.Add(new SelectListItem { Text = item.ilce, Value = item.id.ToString() });
            }

            ViewBag.ilce = l;
            return View();
        }


        [HttpPost]
        public JsonResult deviret(int id, string telefon = "")
        {
            tblSaha saha = db.tblSaha.Where(d => d.sahaId == id).FirstOrDefault();
            if (saha!=null)
            {
                telefon = telefon.Replace("(", "");
                telefon = telefon.Replace(")", "");
                telefon = telefon.Replace("-", "");
                telefon = telefon.Replace(" ", "");

                int kul = int.Parse(Session["golliglogin"].ToString());
                var kontrol = db.tblSahaKullanici.Where(x => x.saha == saha.sahaId && x.panelKullanici == kul).FirstOrDefault();
                if (kontrol!=null)
                {
                    var hedefKul = db.tblKullanici.Where(x => x.telefonNo == telefon).FirstOrDefault();
                    if (hedefKul!=null)
                    {
                        kontrol.panelKullanici = hedefKul.kullaniciId;
                        db.SaveChanges();

                        return Json(new { basarili = true, text = "Devir işlemi "+hedefKul.adSoyad+" isimli kullanıcıya gerçekleştirildi." });
                    }
                    else
                    {
                        return Json(new { basarili = false, text = "Girilen telefon numarasına ait kullanıcı bulunamadı." });
                    }
                }
                else
                {
                    return Json(new { basarili = false, text = "Sahaya erişim yetkiniz yok." });
                }

            }

            return Json(new { basarili = false, text = "Devir işlemi gerçekleştirilemedi." });
        }



        [HttpPost]
            //[ValidateAntiForgeryToken]
        public JsonResult aktiflestir(int id, int durum=1)
            {
                try
                {
                    tblSaha saha = db.tblSaha.Where(d => d.sahaId == id).FirstOrDefault();
                    if (saha != null)
                    {
                        saha.aktif = durum;
                        db.SaveChanges();
                        if (durum==1)
                        {
                             //saha.tblSahaKullanici.FirstOrDefault().tblKullanici;
                            var x = db.tblSahaKullanici.Where(z => z.saha == saha.sahaId).FirstOrDefault().tblKullanici;
                                try
                                {
                                    sahaSahibineMailGonder(saha.adi, x);
                                }
                                catch (Exception)
                                { }

                            return Json(new { basarili = true, text = "Saha aktif hale getirilmiştir." });

                        }
                        else
                        {
                            return Json(new { basarili = true, text = "Saha pasif hale getirilmiştir." });
                        }
                    }
                    else
                    {
                        return Json(new { basarili = false, text = "Saha aktifleştirme sırasında hata oluştu.Sahaya erişilemedi." });
                    }
                }
                catch (Exception)
                {
                    return Json(new { basarili = false, text = "Saha aktifleştirme sırasında hata oluştu." });
                }
            }








        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult Create([Bind(Include = "sahaId,adi,iletisimNo,tipi,adres,aciklama,il,ilce,aktif,en,boy,kapasite,dusVar,dusAdet,sicakSuVar,kramponKiralamaVar,kramponFiyat,ikram,soyunmaOdasiVar,soyunmaOdasiAdet,videoCekimVar,videoCekimFiyat,hakemVar,hakemFiyat,kafeteryaVar,otoparkVar,oroparkFiyat,oyunSalonuVar,wifiVar,jeneratorVar,kaporaIstiyorum,iban,tcVergiNo,vergiLevhasiOnay")] tblSaha tblSaha, 
            HttpPostedFileBase foto1, HttpPostedFileBase foto2, HttpPostedFileBase foto3, HttpPostedFileBase foto4, HttpPostedFileBase foto5)
        {
            if (tblSaha.iletisimNo!=null)
            { 
                tblSaha.iletisimNo = tblSaha.iletisimNo.Replace("(", "");
                tblSaha.iletisimNo = tblSaha.iletisimNo.Replace(")", "");
                tblSaha.iletisimNo = tblSaha.iletisimNo.Replace(" ", "");
                tblSaha.iletisimNo = tblSaha.iletisimNo.Replace("-", "");
            }

            if (ModelState.IsValid && tblSaha.ilce!=null && tblSaha.il!=null)
            {
                if (tblSaha.vergiLevhasiOnay == null || tblSaha.adi == null || tblSaha.tcVergiNo == null || !(tblSaha.ilce > 0 && tblSaha.il > 0))
                {
                    ViewBag.StartupScript = "alert('Doldurulması zorunlu alanları doldurunuz.');";
                }
                else
                {
                    tblSaha = db.tblSaha.Add(tblSaha);
                    db.SaveChanges();

                    tblSaha.foto1 = resmikaydet(foto1, tblSaha.sahaId.ToString() + "-1");
                    tblSaha.foto2 = resmikaydet(foto2, tblSaha.sahaId.ToString() + "-2");
                    tblSaha.foto3 = resmikaydet(foto3, tblSaha.sahaId.ToString() + "-3");
                    tblSaha.foto4 = resmikaydet(foto4, tblSaha.sahaId.ToString() + "-4");
                    tblSaha.foto5 = resmikaydet(foto5, tblSaha.sahaId.ToString() + "-5");

                    db.SaveChanges();

                    var saatler = db.tblRezervasyonSaatleri.ToList();
                    foreach (var item in saatler)
                    {
                        tblFiyat f = new tblFiyat();
                        f.saha = tblSaha.sahaId;
                        f.rezSaati = item.rezId;
                        f.fiyat = 0;
                        db.tblFiyat.Add(f);

                    }
                    db.SaveChanges();

                    int id = int.Parse(Session["golliglogin"].ToString());
                    tblSahaKullanici sk = new tblSahaKullanici();
                    sk.saha = tblSaha.sahaId;
                    sk.panelKullanici = id;
                    db.tblSahaKullanici.Add(sk);
                    db.SaveChanges();

                    yonetimeMailGonder(tblSaha.adi, tblSaha.adres,tblSaha.tcVergiNo, tblSaha.vergiLevhasiOnay);

                    return Json(new { basarili = true, text = "Saha ekleme talebiniz alınmıştır. Sahanız GolLig yetkililerinin onayından sonra yayına alınacaktır. Teşekkür ederiz." });
                }
                
            }
             
            IlcerleriGetir(0);
            ViewBag.il = new SelectList(db.iller, "id", "sehir", tblSaha.il);
            ViewBag.tipi = new SelectList(db.tblSahaTipi, "sahaTipId", "aciklama", tblSaha.tipi);
            return Json(new { basarili = false, text = "işlem başarısız" });
        }

        private void yonetimeMailGonder(string adi, string adres, string tcVergiNo, string vergiLevhasiOnay)
        {
            try
            {
                // var x = vt.tblKullanici.Where(z => z.telefonNo == telefon).FirstOrDefault();
                var x = db.tblSiteBilgileri.FirstOrDefault();
                if (x != null)
                {
                    var body = "<p>Saha Adı: "+adi+"</p>"+
                        "<p>Adres: "+adres+"</p>"+
                        "<p>Tc/Vergi No: " + tcVergiNo + "</p>" +
                        "<p>Vergi Levhası Onay Kodu: " + vergiLevhasiOnay + "</p>" +
                        "<p>www.gollig.com</p>";
                    var message = new MailMessage();
                    message.To.Add(new MailAddress(x.mail));
                    message.From = new MailAddress("info@gollig.com");
                    message.Subject = "GolLig | Yeni Saha Kaydı";
                    message.Body = (body);
                    message.IsBodyHtml = true;

                    NetworkCredential netCred = new NetworkCredential("info@gollig.com", "GolLig2016");

                    SmtpClient smtpobj = new SmtpClient("gollig.com");
                    smtpobj.EnableSsl = false;
                    smtpobj.Credentials = netCred;
                    message.IsBodyHtml = true;


                    smtpobj.Send(message);

                   // return Json(new { basarili = true, text = "Şifreniz mail adresinize gönderildi." });
                }
               // return Json(new { basarili = false, text = "Bu telefon numarasına bağlı bir hesap bulunamadı." });
            }
            catch (Exception ex)
            {
                //return Json(new { basarili = false, text = "İşlem gerçekleşirken beklenmedik bir hata oluştu.Lütfen tekrar deneyin." });
            }
        }

        private void sahaSahibineMailGonder(string adi,tblKullanici x)
        {
            try
            {
                //var x = vt.tblKullanici.Where(z => z.telefonNo == telefon).FirstOrDefault();
                //var x = db.tblSiteBilgileri.FirstOrDefault();
                if (x != null)
                {
                    var body = "<p>" + adi + " isimli halı sahanız aktifleştirilmiştir.</p>" +
                        "<p>www.gollig.com</p>";
                    var message = new MailMessage();
                    message.To.Add(new MailAddress(x.yetkiliMail));
                    message.From = new MailAddress("info@gollig.com");
                    message.Subject = "GolLig | Halı Saha Onayı";
                    message.Body = (body);
                    message.IsBodyHtml = true;

                    NetworkCredential netCred = new NetworkCredential("info@gollig.com", "GolLig2016");

                    SmtpClient smtpobj = new SmtpClient("gollig.com");
                    smtpobj.EnableSsl = false;
                    smtpobj.Credentials = netCred;
                    message.IsBodyHtml = true;


                    smtpobj.Send(message);

                    // return Json(new { basarili = true, text = "Şifreniz mail adresinize gönderildi." });
                }
                // return Json(new { basarili = false, text = "Bu telefon numarasına bağlı bir hesap bulunamadı." });
            }
            catch (Exception ex)
            {
                //return Json(new { basarili = false, text = "İşlem gerçekleşirken beklenmedik bir hata oluştu.Lütfen tekrar deneyin." });
            }
        }


        string resmikaydet(HttpPostedFileBase uri , string yeniAd)
        {
            if (uri!=null)
            {
                if (Path.GetExtension(uri.FileName).Contains("png") || Path.GetExtension(uri.FileName).Contains("jpeg") ||
                     Path.GetExtension(uri.FileName).Contains("jpg"))
                {
                    string dosyaAdi = yeniAd;
                    string filePath = Path.Combine(Server.MapPath("~/img"), dosyaAdi + Path.GetExtension(uri.FileName));
                    uri.SaveAs(filePath);
                    return dosyaAdi + Path.GetExtension(uri.FileName);
                }
            }
            return null;
        }

         


        // GET: YonetimPaneli/Saha/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblSaha tblSaha = db.tblSaha.Find(id);
            if (tblSaha == null)
            {
                return HttpNotFound();
            }
            ViewBag.ilce = new SelectList(db.ilceler, "id", "ilce", tblSaha.ilce);
            ViewBag.il = new SelectList(db.iller, "id", "sehir", tblSaha.il);
            ViewBag.tipi = new SelectList(db.tblSahaTipi, "sahaTipId", "aciklama", tblSaha.tipi);
            return View(tblSaha);
        }

        // POST: YonetimPaneli/Saha/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "sahaId,adi,iletisimNo,tipi,adres,aciklama,il,ilce,aktif,en,boy,kapasite,dusVar,dusAdet,sicakSuVar,kramponKiralamaVar,kramponFiyat,ikram,soyunmaOdasiVar,soyunmaOdasiAdet,videoCekimVar,videoCekimFiyat,hakemVar,hakemFiyat,kafeteryaVar,otoparkVar,oroparkFiyat,oyunSalonuVar,wifiVar,jeneratorVar,kaporaIstiyorum,iban,tcVergiNo,vergiLevhasiOnay")] tblSaha tblSaha,
             HttpPostedFileBase foto1, HttpPostedFileBase foto2, HttpPostedFileBase foto3, HttpPostedFileBase foto4, HttpPostedFileBase foto5)
        {
            if (tblSaha.iletisimNo != null)
            {
                tblSaha.iletisimNo = tblSaha.iletisimNo.Replace("(", "");
                tblSaha.iletisimNo = tblSaha.iletisimNo.Replace(")", "");
                tblSaha.iletisimNo = tblSaha.iletisimNo.Replace(" ", "");
                tblSaha.iletisimNo = tblSaha.iletisimNo.Replace("-", "");
            }

            if (ModelState.IsValid)
            {
                if (foto1!=null)
                {
                    tblSaha.foto1 = resmikaydet(foto1, tblSaha.sahaId.ToString() + "-1");
                }
                if (foto2 != null)
                {
                    tblSaha.foto2 = resmikaydet(foto2, tblSaha.sahaId.ToString() + "-2");
                }
                if (foto3 != null)
                {
                    tblSaha.foto3 = resmikaydet(foto3, tblSaha.sahaId.ToString() + "-3");
                }
                if (foto4 != null)
                {
                    tblSaha.foto4 = resmikaydet(foto4, tblSaha.sahaId.ToString() + "-4");
                }
                if (foto5 != null)
                {
                    tblSaha.foto5 = resmikaydet(foto5, tblSaha.sahaId.ToString() + "-5");
                }
                 

                db.Entry(tblSaha).State = EntityState.Modified;
                db.SaveChanges();


                return RedirectToAction("Index");
            }
            ViewBag.ilce = new SelectList(db.ilceler, "id", "ilce", tblSaha.ilce);
            ViewBag.il = new SelectList(db.iller, "id", "sehir", tblSaha.il);
            ViewBag.tipi = new SelectList(db.tblSahaTipi, "sahaTipId", "aciklama", tblSaha.tipi);
            return View(tblSaha);
        }

        // GET: YonetimPaneli/Saha/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblSaha tblSaha = db.tblSaha.Find(id);
            if (tblSaha == null)
            {
                return HttpNotFound();
            }
            return View(tblSaha);
        }

        
        [HttpPost] 
        public JsonResult Delete(int id)
        {

            try
            {
                tblSaha tblSaha = db.tblSaha.Find(id);
                int kulid = int.Parse(Session["golliglogin"].ToString());
                

                if (tblSaha==null)
                {
                    return Json(new { basarili = false, text = "Saha bulunamadı!" });
                }

                var kontrol = tblSaha.tblSahaKullanici.Where(z => z.panelKullanici == kulid).FirstOrDefault();
                if (kontrol==null)
                {
                    return Json(new { basarili = false, text = "Sahaya ait yetkiniz bulunamadı!" });
                }
                   
                db.tblFiyat.RemoveRange(db.tblFiyat.Where(x => x.saha == id).ToList());
                db.tblRezervasyon.RemoveRange(db.tblRezervasyon.Where(x => x.saha.Value == id).ToList());
                db.tblSahaKullanici.RemoveRange(db.tblSahaKullanici.Where(x => x.saha == id).ToList());


               
                db.tblSaha.Remove(tblSaha);
                db.SaveChanges();

                
                return Json(new { basarili = true, text = "Saha silme işlemi gerçekleştirilmiştir." });
            }
            catch (Exception ex)
            {
                return Json(new { basarili = false, text = "Saha silenemedi!" });
            } 
             
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
