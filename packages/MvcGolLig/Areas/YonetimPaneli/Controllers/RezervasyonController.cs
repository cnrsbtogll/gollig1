﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcGolLig.Models;
using System.Net.Mail;

namespace MvcGolLig.Areas.YonetimPaneli.Controllers
{
    public class RezervasyonController : Controller
    {
        private vtGolLigEntities db = new vtGolLigEntities();

        // GET: YonetimPaneli/Rezervasyon
        public ActionResult Index()
        { 
            try
            {
                int id = int.Parse(Session["golliglogin"].ToString());
                //giriş yapan kullanıcının yetkisi olan sahalar
                var sahalar = db.tblSahaKullanici.Where(c => c.panelKullanici == id).Select(c => c.saha);

                var tblRezervasyon = db.tblRezervasyon.Where(x => x.durum!=-1 && sahalar.Contains(x.saha)).Include(t => t.tblFiyat).Include(t => t.tblRezervasyonSaatleri).Include(t => t.tblSaha).Include(t => t.tblKullanici)
                    .OrderBy(c=>c.durum).ToList();

                var tblRezervasyon2 = db.tblRezervasyon.Where(x => x.durum == -1 && sahalar.Contains(x.saha)).Include(t => t.tblFiyat).Include(t => t.tblRezervasyonSaatleri).Include(t => t.tblSaha).Include(t => t.tblKullanici)
                    .ToList();

                tblRezervasyon.AddRange(tblRezervasyon2.ToList());

                return View(tblRezervasyon.ToList());
            }
            catch (Exception)
            {
                return Redirect("/yonetimpaneli");
            }
        }

        // GET: YonetimPaneli/Rezervasyon/Details/5
        public ActionResult Details(int id=-1,string kul="",string tarih="")
        {
            if (id!=-1)
            {
                tblRezervasyon tblRezervasyon = db.tblRezervasyon.Where(x => x.rezId == id).FirstOrDefault();
                if (tblRezervasyon == null)
                {
                    return Redirect("/yonetimpaneli");
                }
                return View(tblRezervasyon);
            }
            else if (kul!="" && tarih!="")
            {
                try
                {
                    DateTime pTarih = DateTime.Parse(tarih);

                    var tblRezervasyon= db.tblRezervasyon.Where(x => x.tblKullanici.adSoyad + " " + x.tblKullanici.takimAdi == kul && x.tarih == pTarih && x.durum!=-1).FirstOrDefault();
                    if (tblRezervasyon == null)
                    {
                        return Redirect("/yonetimpaneli");
                    }
                    return View(tblRezervasyon); 
                }
                catch (Exception ex)
                { return Redirect("/yonetimpaneli"); }
            }
            else
            {
                return Redirect("/yonetimpaneli");
            }
             
            return Redirect("/yonetimpaneli");
        }

        // GET: YonetimPaneli/Rezervasyon/Create
        
        public ActionResult Create(string rezSaati, string tarih)
        {
            //ViewBag.fiyatId = new SelectList(db.tblFiyat, "fiyatId", "fiyat");
            SelectList saatListe= new SelectList(db.tblRezervasyonSaatleri, "rezId", "aciklama");
            ViewBag.rezSaati = saatListe;

            SelectList listeSaha=null;
            try
            {
                int id = int.Parse(Session["golliglogin"].ToString());
                int tip = int.Parse(Session["golliglogintip"].ToString());
                if (tip == 100)
                {
                    var sahalar = db.tblSahaKullanici.Where(c => c.panelKullanici == id).Select(c => c.saha);

                    var tblSaha = db.tblSaha.Where(x => sahalar.Contains(x.sahaId)).Include(t => t.ilceler).Include(t => t.iller).Include(t => t.tblSahaTipi);
                    listeSaha= new SelectList(tblSaha, "sahaId", "adi");
                    ViewBag.saha = listeSaha;
                }
                else
                {
                    ViewBag.saha = new SelectList(db.tblSaha, "sahaId", "adi");
                }
            }
            catch (Exception)
            {
                Response.Redirect("/yonetimpaneli");
            }

            if (rezSaati!=null)
            {
                ViewBag.rezSaatiText = rezSaati;

                
                foreach (var item in saatListe)
                {
                    if (item.Text==rezSaati)
                    {
                        ViewBag.rezSaatId = item.Value;
                        break;
                    }
                }

            }
            

            ViewBag.takim = new SelectList(db.tblKullanici, "kullaniciId", "adSoyad");
            return View();
        }


        string tarihSaatKontrolEt(string saat,DateTime pTarih)
        {
            string saatBas = saat.Split('-')[0];

            double saatFarki = ((DateTime.Parse(saatBas)) - DateTime.Now).TotalHours;
            TimeSpan GunFarki = DateTime.Now.Subtract(pTarih);


            if (pTarih.ToShortDateString() == DateTime.Now.ToShortDateString())
            {
                if (saatFarki > 0)
                {//bugüne kayıt yapılacak saati kontrol et
                    return "1";
                }
                else
                {
                    return "Geçmiş saate rezervasyon alamazsınız!";
                }
            }
            else if (GunFarki.TotalDays>=1)
            {
                return "Geçmiş tarihe rezervasyon alamazsınız!";
            }
            else
            {
                return "1";
            }
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "rezId,saha,takim,aciklama,tarih,fiyatId,fiyat,durum,rezSaati")] tblRezervasyon tblRezervasyon)
        {
            ViewBag.rezSaati = new SelectList(db.tblRezervasyonSaatleri, "rezId", "aciklama", tblRezervasyon.rezSaati);
            ViewBag.saha = new SelectList(db.tblSaha, "sahaId", "adi", tblRezervasyon.saha);
            ViewBag.takim = new SelectList(db.tblKullanici, "kullaniciId", "adSoyad", tblRezervasyon.takim);

            if (ModelState.IsValid)
            {
                if (tblRezervasyon.tarih==null)
                {
                    ViewBag.StartupScript = "alert('Tarih müsait değil.');";
                    return View(tblRezervasyon);
                }
                else
                {
                    var rezSaatiText = db.tblRezervasyonSaatleri.Where(x => x.rezId == tblRezervasyon.rezSaati).FirstOrDefault();
                    if (rezSaatiText == null) return View(tblRezervasyon); 

                    string tarihSaatKontrol = tarihSaatKontrolEt(rezSaatiText.aciklama, tblRezervasyon.tarih.Value);
                    if (tarihSaatKontrol=="1")
                    {
                        var kontrol = db.tblRezervasyon.Where(x => x.tarih == tblRezervasyon.tarih && x.saha == tblRezervasyon.saha && x.rezSaati == tblRezervasyon.rezSaati && x.durum != -1).FirstOrDefault();
                        if (kontrol == null)
                        {
                            tblRezervasyon.durum = 1;
                            var fiyat = db.tblFiyat.Where(x => x.saha == tblRezervasyon.saha && x.rezSaati == tblRezervasyon.rezSaati).FirstOrDefault();
                            if (fiyat != null)
                            {
                                tblRezervasyon.fiyatId = fiyat.fiyatId;
                                db.tblRezervasyon.Add(tblRezervasyon);
                                db.SaveChanges();
                                //return RedirectToAction("Index");
                                //return Redirect("/yonetimpaneli");
                                var saha = db.tblSaha.Where(x => x.sahaId == tblRezervasyon.saha).FirstOrDefault();

                                ViewBag.StartupScript = "alert('Rezervasyon Yapıldı!\\n\\nSaha:"+saha.adi+ "\\n\\nTarih:" + tblRezervasyon.tarih.Value.ToShortDateString()+ "\\n\\nSaat:" + rezSaatiText.aciklama + " '); window.location = '/yonetimpaneli';";
                                return View(tblRezervasyon);
                            }
                            ViewBag.StartupScript = "alert('Fiyat detayına ulaşılamadı.');";
                            //return View(tblRezervasyon);
                        }
                        else
                        {
                            ViewBag.StartupScript = "alert('Seçtiğiniz tarihler müsait değil.');";
                            //return View(tblRezervasyon);
                        }
                    }
                    else
                    {
                        ViewBag.StartupScript = "alert('"+ tarihSaatKontrol + "');";
                        return View(tblRezervasyon);
                    }

                    
                }
                 
            }

            //ViewBag.fiyatId = new SelectList(db.tblFiyat, "fiyatId", "fiyatId", tblRezervasyon.fiyatId);
            
            return View(tblRezervasyon);
        }

        // GET: YonetimPaneli/Rezervasyon/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblRezervasyon tblRezervasyon = db.tblRezervasyon.Find(id);
            if (tblRezervasyon == null)
            {
                return HttpNotFound();
            }
            ViewBag.fiyatId = new SelectList(db.tblFiyat, "fiyatId", "fiyatId", tblRezervasyon.fiyatId);
            ViewBag.rezSaati = new SelectList(db.tblRezervasyonSaatleri, "rezId", "aciklama", tblRezervasyon.rezSaati);
            ViewBag.saha = new SelectList(db.tblSaha, "sahaId", "adi", tblRezervasyon.saha);
            ViewBag.takim = new SelectList(db.tblKullanici, "kullaniciId", "adSoyad", tblRezervasyon.takim);
            return View(tblRezervasyon);
        }

        // POST: YonetimPaneli/Rezervasyon/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "rezId,saha,takim,aciklama,tarih,fiyatId,fiyat,durum,rezSaati")] tblRezervasyon tblRezervasyon)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblRezervasyon).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.fiyatId = new SelectList(db.tblFiyat, "fiyatId", "fiyatId", tblRezervasyon.fiyatId);
            ViewBag.rezSaati = new SelectList(db.tblRezervasyonSaatleri, "rezId", "aciklama", tblRezervasyon.rezSaati);
            ViewBag.saha = new SelectList(db.tblSaha, "sahaId", "adi", tblRezervasyon.saha);
            ViewBag.takim = new SelectList(db.tblKullanici, "kullaniciId", "adSoyad", tblRezervasyon.takim);
            return View(tblRezervasyon);
        }

        // GET: YonetimPaneli/Rezervasyon/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblRezervasyon tblRezervasyon = db.tblRezervasyon.Find(id);
            if (tblRezervasyon == null)
            {
                return HttpNotFound();
            }
            return View(tblRezervasyon);
        }

        // POST: YonetimPaneli/Rezervasyon/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblRezervasyon tblRezervasyon = db.tblRezervasyon.Find(id);
            //db.tblRezervasyon.Remove(tblRezervasyon);

            tblRezervasyon.durum = -1;

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        

            [HttpPost] 
            public JsonResult DurumDegistir(int id, int durum = 1)
            {
                try
                {
                    tblRezervasyon rez = db.tblRezervasyon.Where(d => d.rezId == id).FirstOrDefault();
                int userid = int.Parse(Session["golliglogin"].ToString());
                var yetki=db.tblSahaKullanici.Where(x => x.saha == rez.saha && x.panelKullanici == userid).FirstOrDefault();
                if (yetki==null)
                {
                    return Json(new { basarili = false, text = "Bu rezervasyonu güncelleme yetkiniz yok." });
                }    

                    if (rez != null)
                    {
                        rez.durum = durum;
                        db.SaveChanges();
                        var x = rez.tblKullanici;
                        if (durum == 1)
                        { 
                            
                            rezervasyonSahibineMailGonder(durum,x,rez); 

                            return Json(new { basarili = true, text = "Rezervasyon onaylandı." });

                        }
                        else
                        {
                            rezervasyonSahibineMailGonder(durum, x, rez);
                            return Json(new { basarili = true, text = "Rezervasyon iptal edildi." });
                        }
                    }
                    else
                    {
                        return Json(new { basarili = false, text = "Rezervasyon güncelleme sırasında hata oluştu." });
                    }
                }
                catch (Exception)
                {
                    return Json(new { basarili = false, text = "Rezervasyon güncelleme sırasında hata oluştu." });
                }
            }

        private void rezervasyonSahibineMailGonder(int durum, tblKullanici x, tblRezervasyon rez)
        {
            try
            {
                //var x = vt.tblKullanici.Where(z => z.telefonNo == telefon).FirstOrDefault();
                //var x = db.tblSiteBilgileri.FirstOrDefault();
                if (x != null)
                {
                    var body = "";
                    var message = new MailMessage();
                    message.To.Add(new MailAddress(x.yetkiliMail));
                    message.From = new MailAddress("info@gollig.com");
                    if (durum==1)
                    {
                        body = "<p>Merhaba "+x.adSoyad+",</p><p>"+rez.tblSaha.adi+" isimli halı sahasına yaptığın "+rez.tarih.Value.ToString("dd.MM.yyyy")+" "+rez.tblRezervasyonSaatleri.aciklama+ " tarihli rezervasyonun onaylanmıştır.</p>" +
                       "<p>www.gollig.com</p>";
                        message.Subject = "GolLig | Rezervasyonun Onaylandı!";
                    }
                    else
                    {
                        body = "<p>Merhaba " + x.adSoyad + ",</p><p>" + rez.tblSaha.adi + " isimli halı sahasına yaptığın " + rez.tarih.Value.ToString("dd.MM.yyyy") + " " + rez.tblRezervasyonSaatleri.aciklama + " tarihli rezervasyonun iptal edilmiştir."+
                            " </p>" +
                       "<p>www.gollig.com</p>";
                        message.Subject = "GolLig | Rezervasyonun Onaylanmadı!";
                    } 

                    
                    message.Body = (body);
                    message.IsBodyHtml = true;

                    NetworkCredential netCred = new NetworkCredential("info@gollig.com", "GolLig2016");

                    SmtpClient smtpobj = new SmtpClient("gollig.com");
                    smtpobj.EnableSsl = false;
                    smtpobj.Credentials = netCred;
                    message.IsBodyHtml = true;


                    smtpobj.Send(message);

                    // return Json(new { basarili = true, text = "Şifreniz mail adresinize gönderildi." });
                }
                // return Json(new { basarili = false, text = "Bu telefon numarasına bağlı bir hesap bulunamadı." });
            }
            catch (Exception ex)
            {
                //return Json(new { basarili = false, text = "İşlem gerçekleşirken beklenmedik bir hata oluştu.Lütfen tekrar deneyin." });
            }
        }
    }
}
