﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcGolLig.Models;

namespace MvcGolLig.Areas.YonetimPaneli.Controllers
{
    public class SiteBilgileriController : Controller
    {
        private vtGolLigEntities db = new vtGolLigEntities();

        // GET: YonetimPaneli/SiteBilgileri
        public ActionResult Index()
        {
            var sb= db.tblSiteBilgileri.FirstOrDefault();
            if (sb!=null)
            {
                return RedirectToAction("Edit/"+sb.bilgiId);
            }
            else
            {
                return Redirect("/yonetimpaneli");
            }
            //return View(db.tblSiteBilgileri.ToList());
        }

       
         
        public ActionResult Create()
        {
            return View();
        }
         
         
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblSiteBilgileri tblSiteBilgileri = db.tblSiteBilgileri.FirstOrDefault();
            if (tblSiteBilgileri == null)
            {
                return HttpNotFound();
            }
            return View(tblSiteBilgileri);
        }
         
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "bilgiId,aciklama,telefon,mail,adres")] tblSiteBilgileri tblSiteBilgileri)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblSiteBilgileri).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblSiteBilgileri);
        }

        

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
