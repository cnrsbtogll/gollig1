﻿using MvcGolLig.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcGolLig.Areas.YonetimPaneli.Controllers
{
    public class MenuController : Controller
    {
        vtGolLigEntities vt = new vtGolLigEntities();
        // GET: YonetimPaneli/Menu
        public ActionResult Index()
        {
            try
            {
                if (Session["golliglogin"] != null)
                {
                    ViewBag.Normal = vt.tblMenu.Where(x => x.kullaniciTipi == 100).ToList();

                    int tip = int.Parse(Session["golliglogintip"].ToString());
                    if (tip==100)
                    { 

                        return PartialView(vt.tblMenu.Where(x => x.kullaniciTipi == tip).ToList());
                    }
                    else
                    {
                        ViewBag.Admin = vt.tblMenu.Where(x => x.kullaniciTipi == 0).ToList();
                        return PartialView(vt.tblMenu.ToList());
                    }
                }
                else
                {
                    return Redirect("/giris");
                }
            }
            catch (Exception)
            {
                return Redirect("/giris");
            }
             
        }
    }
}