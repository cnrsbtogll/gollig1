﻿using MvcGolLig.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcGolLig.Controllers
{

    public class HomeController : Controller
    {
        vtGolLigEntities vt = new vtGolLigEntities();
        // GET: Home
        [RequireHttps]
        public ActionResult Index(string Tarih, string Saatler, string Sehirler, string Ilceler)
        {
            ViewBag.Bugun = DateTime.Now.ToShortDateString();
            sehirleriYukle();
            saatleriYukle();
            if (String.IsNullOrEmpty(Sehirler))
            {
                IlcerleriGetir(0);
            }
            else
            {
                IlcerleriGetir(int.Parse(Sehirler));
            }
            sahaBulButtonText();

            if (Tarih == null) Tarih = DateTime.Now.ToString("dd.MM.yyyy");
            else if (Tarih == "") Tarih = DateTime.Now.ToString("dd.MM.yyyy");

            try
            {
                DateTime pTarih = DateTime.Parse(Tarih);
                int pSaat = int.Parse(Saatler);
                int pSehir = int.Parse(Sehirler);
                int pIlce = int.Parse(Ilceler);

                ViewBag.Bugun = pTarih.ToShortDateString();

                TimeSpan fark = pTarih.Date- DateTime.Now;
                if (fark.TotalDays>10)
                {
                    ViewBag.StartupScript = "alert('10 günden daha ileriye bir rezervasyon yapamazsınız!');";
                    List<vw_FiyatListesi> list = vt.vw_FiyatListesi.Take(2).ToList();

                    return View(list);
                }
                else if (fark.TotalDays<-1)
                {
                    ViewBag.StartupScript = "alert('Geçmiş tarihli rezervasyon yapamazsınız!');";
                    List<vw_FiyatListesi> list = vt.vw_FiyatListesi.Take(2).ToList();

                    return View(list);
                }

                var rezler = vt.tblRezervasyon.Where(r => r.tarih.Value == pTarih).Select(e => e.fiyatId);
                List<vw_FiyatListesi> Sahalar = new List<vw_FiyatListesi>();


                if (pIlce!=-1 && pSaat!=-1)
                {//ilçe filtresi var, saat filtresi var
                    Sahalar = vt.vw_FiyatListesi.Where(x => !rezler.Contains(x.KOD) && x.il == pSehir && x.ilce == pIlce && x.rezId == pSaat ).ToList();
                }
                else if (pIlce == -1 && pSaat == -1 && pSehir!=-1)
                {//ilçe filtresi yok, saat filtresi yok
                    Sahalar = vt.vw_FiyatListesi.Where(x => !rezler.Contains(x.KOD) && x.il == pSehir).ToList();
                } 
                else if (pIlce!=-1 && pSaat==-1)
                {//ilçe filtresi var, saat filtresi yok
                    Sahalar = vt.vw_FiyatListesi.Where(x => !rezler.Contains(x.KOD) && x.il == pSehir && x.ilce == pIlce).ToList();
                }
                else if (pIlce == -1 && pSaat != -1 && pSehir==-1)
                {//il - ilçe filtresi yok, saat filtresi var
                    Sahalar = vt.vw_FiyatListesi.Where(x => !rezler.Contains(x.KOD) && x.rezId == pSaat).ToList();
                }
                else if (pSehir == -1 && pIlce==-1 && pTarih!=null && pSaat!=-1)
                {
                    Sahalar = vt.vw_FiyatListesi.Where(x => !rezler.Contains(x.KOD) && x.rezId == pSaat).ToList();
                }
                else if (pSehir == -1 && pIlce == -1 && pTarih != null && pSaat == -1)
                {
                    ViewBag.StartupScript = "alert('Başarılı sonuçlar için daha detaylı arama yapabilirsiniz.');";
                    Sahalar = vt.vw_FiyatListesi.Where(x => !rezler.Contains(x.KOD)).Take(15).ToList();
                }
                else if (pSehir!=-1 && pTarih!=null && pSaat!=-1)
                {
                    Sahalar = vt.vw_FiyatListesi.Where(x => !rezler.Contains(x.KOD) && x.il == pSehir &&  x.rezId == pSaat).ToList();
                }

                return View(Sahalar);
            }
            catch (Exception)
            {
                //ViewBag.Ilceler = new SelectList(vt.ilceler.Where(x => x.sehir == -1).ToList(), "id", "ilce");
            }
            

           // List<vw_FiyatListesi> l = vt.vw_FiyatListesi.Take(10).ToList();


            List<vw_FiyatListesi> l = vt.vw_FiyatListesi.Where(x=>x.SAAT== "20:00-21:00").OrderBy(x=>x.fiyat).Take(10).ToList();


            

            return View(l);
        }

        void sahaBulButtonText()
        {
            try
            {
                int kul = int.Parse(Session["golliglogin"].ToString());

                var yetkiler= vt.tblSahaKullanici.Where(x => x.panelKullanici == kul).Select(x=>x.saha);

                var onayliSahalar=vt.tblSaha.Where(x => yetkiler.Contains(x.sahaId) && x.aktif == 1).ToList();

                if (onayliSahalar.Count>0)
                {
                    ViewBag.ButtonText = "Maç Sat";
                    ViewBag.ButtonLink = "/yonetimpaneli";
                }
                else
                {
                    ViewBag.ButtonText = "Saha Ekle";
                    ViewBag.ButtonLink = "/yonetimpaneli/saha/create";
                }
            }
            catch (Exception)
            {
                ViewBag.ButtonText = "Maç Sat";
                ViewBag.ButtonLink = "/yonetimpaneli";
            }
        }



        void sehirleriYukle()
        {
            List<SelectListItem> sehirList = new List<SelectListItem>();
            sehirList.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });
            foreach (var item in vt.iller.ToList())
            {
                sehirList.Add(new SelectListItem { Text = item.sehir, Value = item.id.ToString() });
            }
            ViewBag.Sehirler = sehirList;
        }

        void saatleriYukle()
        {
            List<SelectListItem> saatList = new List<SelectListItem>();
            saatList.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });
            foreach (var item in vt.tblRezervasyonSaatleri.ToList())
            {
                saatList.Add(new SelectListItem { Text = item.aciklama, Value = item.rezId.ToString() });
            }
            ViewBag.Saatler = saatList;
        }
          
        public ActionResult IlcerleriGetir(int id=0)
        {
            List<SelectListItem> l = new List<SelectListItem>();
            l.Add(new SelectListItem { Text = "Hepsi", Value = "-1" });

            foreach (var item in vt.ilceler.Where(x => x.sehir == id).ToList())
            {
                l.Add(new SelectListItem {Text=item.ilce, Value=item.id.ToString() });
            }
            
            ViewBag.Ilceler = l;
            return View();
        }


        public ActionResult Sahalar()
        {
            return View(vt.tblSaha.ToList());
        }


        public ActionResult cikis()
        {
            Session.Abandon();
            return RedirectToAction("Index");// Redirect("/");
        }

    }
}