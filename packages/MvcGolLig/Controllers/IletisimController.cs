﻿using MvcGolLig.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MvcGolLig.Controllers
{
    public class IletisimController : Controller
    {
        vtGolLigEntities vt = new vtGolLigEntities();
        // GET: Iletisim
        public ActionResult Index(string isim,string mail,string mesaj)
        {
            var bilgiler = vt.tblSiteBilgileri.FirstOrDefault();
            if (bilgiler!=null)
            {
                ViewBag.adres = bilgiler.adres;
                ViewBag.telefon = bilgiler.telefon;
                ViewBag.mail = bilgiler.mail;
                ViewBag.aciklama = bilgiler.aciklama;
            }
            else
            {
                return Redirect("/");
            }

            if (isim!=null && mail!=null && mesaj!=null)
            {
                gonder(isim,mail,mesaj,bilgiler.mail);
            }

            return View();
        }

        private void gonder(string isim, string mail, string mesaj,string hedef)
        {
            try
            {
                var body = "<p>Gönderici: {0} ({1})</p><p>Mesaj:</p><p>{2}</p>";
                var message = new MailMessage();
                message.To.Add(new MailAddress(hedef));  // replace with valid value 
                message.From = new MailAddress("iletisim@gollig.com");  // replace with valid value
                message.Subject = "GolLig_Iletisim";
                message.Body = string.Format(body, isim, mail, mesaj);
                message.IsBodyHtml = true;

                NetworkCredential netCred = new NetworkCredential("iletisim@gollig.com", "GolLig2016");

                SmtpClient smtpobj = new SmtpClient("gollig.com");
                smtpobj.EnableSsl = false;
                smtpobj.Credentials = netCred;
                message.IsBodyHtml = true;


                smtpobj.Send(message);
            }
            catch (Exception)
            {
                 
            }

        }
    }
}